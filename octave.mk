OCTAVE_HG_URL = https://www.octave.org/hg/octave
OCTAVE_CONFIGURE_FLAGS = --config-cache \
  --with-blas=blas --with-lapack=lapack \
  CPPFLAGS='-DLONGBLAS=long -DBLAS64' \
  F77_INTEGER_8_FLAG='-fdefault-integer-8' \
  LDFLAGS='-L$(CURDIR)' \
  LD_LIBRARY_PATH="$(CURDIR):$$LD_LIBRARY_PATH"

octave: octave/_build/src/octave

octave/_build/src/octave: octave/_build/Makefile
	LD_LIBRARY_PATH="$(CURDIR):$$LD_LIBRARY_PATH" $(MAKE) -C octave/_build all

octave/_build/Makefile: octave/configure $(SUPPORT_LIBS)
	mkdir -p octave/_build
	cd octave/_build && $(SHELL) ../configure $(OCTAVE_CONFIGURE_FLAGS)

octave/configure: octave/bootstrap
	cd octave && $(SHELL) bootstrap

octave/bootstrap:
	$(HG) clone --quiet $(OCTAVE_HG_URL) octave

CLEANFILES += octave/_build
DISTCLEANFILES += octave

all-octave: octave

check-octave: all-octave
	LD_LIBRARY_PATH="$(CURDIR):$$LD_LIBRARY_PATH" $(MAKE) -C octave/_build check

.PHONY: octave all-octave check-octave
