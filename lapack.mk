LAPACK_DIR = lapack-3.7.1
BLAS_STATICLIB = $(LAPACK_DIR)/librefblas.a
LAPACK_ORIG_SOURCE = lapack_3.7.1.orig.tar.gz
LAPACK_STATICLIB = $(LAPACK_DIR)/liblapack.a
LAPACK_SONUM = 3
LAPACK_TMPDIR = $(LAPACK_DIR)/tmp

LAPACK_SHA1SUM = 84c4f7163b52b1bf1f6ca2193f6f48ed3dec0fab
LAPACK_TIMESTAMP = 20170704T213528Z
LAPACK_URL = $(SNAPSHOT_D_O_URL)/$(LAPACK_TIMESTAMP)/pool/main/l/lapack/$(LAPACK_ORIG_SOURCE)

BLAS_LAPACK = libblas.so liblapack.so

blas: libblas.so

lapack: liblapack.so

libblas.so: libblas.so.$(LAPACK_SONUM)
	ln -sfn $< $@

liblapack.so: liblapack.so.$(LAPACK_SONUM)
	ln -sfn $< $@

libblas.so.$(LAPACK_SONUM): $(BLAS_STATICLIB)
	rm -rf $(LAPACK_TMPDIR)
	mkdir -p $(LAPACK_TMPDIR)
	cp $< $(LAPACK_TMPDIR)
	cd $(LAPACK_TMPDIR) && $(AR) x $(notdir $<)
	cd $(LAPACK_TMPDIR) && $(F77) -shared -Wl,-soname=$@ *.o -o $(CURDIR)/$@
	chmod a-x $@
	rm -rf $(LAPACK_TMPDIR)

liblapack.so.$(LAPACK_SONUM): $(LAPACK_STATICLIB) libblas.so
	rm -rf $(LAPACK_TMPDIR)
	mkdir -p $(LAPACK_TMPDIR)
	cp $< $(LAPACK_TMPDIR)
	cd $(LAPACK_TMPDIR) && $(AR) x $(notdir $<)
	cd $(LAPACK_TMPDIR) && $(F77) -shared -Wl,-soname=$@ *.o -o $(CURDIR)/$@ -L$(CURDIR) -lblas
	chmod a-x $@
	rm -rf $(LAPACK_TMPDIR)

$(BLAS_STATICLIB): $(LAPACK_DIR)/make.inc
	$(MAKE) -C $(LAPACK_DIR)/BLAS/SRC NOOPT="$(FFLAGS_NOOPT)" OPTS="$(FFLAGS)"

$(LAPACK_STATICLIB): $(LAPACK_DIR)/make.inc
	$(MAKE) -C $(LAPACK_DIR)/SRC NOOPT="$(FFLAGS_NOOPT)" OPTS="$(FFLAGS)"

$(LAPACK_DIR)/make.inc: $(LAPACK_ORIG_SOURCE)
	rm -rf $(LAPACK_DIR)
	$(TAR) xf $<
	cp $(LAPACK_DIR)/INSTALL/make.inc.gfortran $@

$(LAPACK_ORIG_SOURCE):
	$(WGET) --quiet -O $@-t $(LAPACK_URL)
	echo '$(LAPACK_SHA1SUM) $@-t' | $(SHA1SUM) --quiet --check && mv $@-t $@ || rm -f $@-t

CLEANFILES += $(LAPACK_DIR) libblas.so* liblapack.so*
DISTCLEANFILES += $(LAPACK_ORIG_SOURCE)
SUPPORT_LIBS += $(BLAS_LAPACK)

.PHONY: blas lapack
