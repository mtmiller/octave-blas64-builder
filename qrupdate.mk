QRUPDATE_DIR = qrupdate-1.1.2
QRUPDATE_ORIG_SOURCE = qrupdate_1.1.2.orig.tar.gz
QRUPDATE_INSTALLDIR = $(QRUPDATE_DIR)/__installdir__
QRUPDATE_LIBDIR = $(QRUPDATE_INSTALLDIR)/usr/local/lib

QRUPDATE_SHA1SUM = f7403b646ace20f4a2b080b4933a1e9152fac526
QRUPDATE_TIMESTAMP = 20120815T032731Z
QRUPDATE_URL = $(SNAPSHOT_D_O_URL)/$(QRUPDATE_TIMESTAMP)/pool/main/q/qrupdate/$(QRUPDATE_ORIG_SOURCE)

qrupdate: libqrupdate.so

libqrupdate.so: $(QRUPDATE_LIBDIR)/libqrupdate.so
	cp -P $<* .
	chmod a-x $@*

$(QRUPDATE_LIBDIR)/libqrupdate.so: $(QRUPDATE_DIR)/.timestamp $(BLAS_LAPACK)
	$(MAKE) -C $(QRUPDATE_DIR) solib FFLAGS='$(FFLAGS)' LDFLAGS='-L$(CURDIR)'
	$(MAKE) -C $(QRUPDATE_DIR) install FFLAGS='$(FFLAGS)' LDFLAGS='-L$(CURDIR)' \
		DESTDIR='$(CURDIR)/$(QRUPDATE_INSTALLDIR)'

$(QRUPDATE_DIR)/.timestamp: $(QRUPDATE_ORIG_SOURCE)
	rm -rf $(QRUPDATE_DIR)
	$(TAR) xf $<
	echo timestamp > $@

$(QRUPDATE_ORIG_SOURCE):
	$(WGET) --quiet -O $@-t $(QRUPDATE_URL)
	echo '$(QRUPDATE_SHA1SUM) $@-t' | $(SHA1SUM) --quiet --check && mv $@-t $@ || rm -f $@-t

CLEANFILES += $(QRUPDATE_DIR) libqrupdate.so*
DISTCLEANFILES += $(QRUPDATE_ORIG_SOURCE)
SUPPORT_LIBS += libqrupdate.so

.PHONY: qrupdate
