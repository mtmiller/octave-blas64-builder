Octave BLAS64 Builder
=====================

This is a project to make it easier to build GNU Octave with 64-bit
indexing on GNU/Linux systems.

Quick Start
-----------

    make all
    LD_LIBRARY_PATH=$PWD octave/_build/run-octave

Why?
----

Why can Octave not just use `size_t` and use large arrays on 64-bit
systems?

Octave depends on many Fortran libraries for its core functionality.
Unfortunately, Fortran does not have the equivalent of the C `size_t`
type, so these math libraries use the standard `INTEGER` type as an
array index. For maximum compatibility with existing code, this is a
32-bit integer by default.

So to build Octave with 64-bit indexes, all Fortran libraries must be
recompiled with a 64-bit `INTEGER` type. This makes the libraries
potentially incompatible with the rest of your OS, but Octave can be
configured to use them.

License
-------

This project is just a bunch of makefile rules to facilitate building a
set of numerical libraries and primarily GNU Octave. The makefiles are
licensed under the GNU GPL v3 or later, see the file [COPYING](COPYING)
for more details.

Each of the numerical libraries is distributed under its own license,
refer to the project sources for more details.

Thanks
------

This project is inspired by [mxe](https://mxe.cc) and
[mxe-octave](https://wiki.octave.org/MXE) but not derived from either of
them.

The [snapshot.debian.org](https://snapshot.debian.org/) service is used
for the source distributions of the numerical libraries.
