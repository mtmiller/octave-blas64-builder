# Makefile for building Octave with 64-bit-integer Fortran support libraries

AR = ar
CC = gcc
CFLAGS = -g -O2 -fPIC
CXX = g++
CXXFLAGS = -g -O2 -fPIC
F77 = gfortran
FFLAGS = -g -O2 -fPIC -fdefault-integer-8
FFLAGS_NOOPT = $(patsubst -O2,-O0,$(FFLAGS))
HG = hg
SHA1SUM = sha1sum
TAR = tar
WGET = wget

SNAPSHOT_D_O_URL = https://snapshot.debian.org/archive/debian

CLEANFILES =
DISTCLEANFILES =
SUPPORT_LIBS =

all: all-octave

check: check-octave

clean:
	-rm -rf $(CLEANFILES)

distclean: clean
	-rm -rf $(DISTCLEANFILES)

## Include package-specific makefiles in dependency order.
include lapack.mk
include arpack.mk
include qrupdate.mk
include suitesparse.mk
include octave.mk

.PHONY: all check clean distclean
