ARPACK_DIR = arpack-ng-3.5.0
ARPACK_ORIG_SOURCE = arpack_3.5.0+real.orig.tar.gz
ARPACK_CONFIGURE_FLAGS = --config-cache \
  --with-blas=blas --with-lapack=lapack \
  F77='$(F77)' FFLAGS='$(FFLAGS)' \
  LDFLAGS='-L$(CURDIR)'
ARPACK_INSTALLDIR = $(ARPACK_DIR)/__installdir__
ARPACK_LIBDIR = $(ARPACK_INSTALLDIR)/usr/local/lib

ARPACK_URL = $(SNAPSHOT_D_O_URL)/$(ARPACK_TIMESTAMP)/pool/main/a/arpack/$(ARPACK_ORIG_SOURCE)
ARPACK_TIMESTAMP = 20170914T160634Z
ARPACK_SHA1SUM = acbbe14b03b323f84236553872396345204dea1e

arpack: libarpack.so

libarpack.so: $(ARPACK_LIBDIR)/libarpack.so
	cp -P $<* .
	chmod a-x $@*

$(ARPACK_LIBDIR)/libarpack.so: $(ARPACK_DIR)/config.status
	$(MAKE) -C $(ARPACK_DIR) all
	$(MAKE) -C $(ARPACK_DIR) install DESTDIR='$(CURDIR)/$(ARPACK_INSTALLDIR)'

$(ARPACK_DIR)/config.status: $(ARPACK_ORIG_SOURCE) $(BLAS_LAPACK)
	rm -rf $(ARPACK_DIR)
	$(TAR) xf $<
	cd $(ARPACK_DIR) && $(SHELL) ./bootstrap && $(SHELL) ./configure $(ARPACK_CONFIGURE_FLAGS)

$(ARPACK_ORIG_SOURCE):
	$(WGET) --quiet -O $@-t $(ARPACK_URL)
	echo '$(ARPACK_SHA1SUM) $@-t' | $(SHA1SUM) --quiet --check && mv $@-t $@ || rm -f $@-t

CLEANFILES += $(ARPACK_DIR) libarpack.so*
DISTCLEANFILES += $(ARPACK_ORIG_SOURCE)
SUPPORT_LIBS += libarpack.so

.PHONY: arpack
